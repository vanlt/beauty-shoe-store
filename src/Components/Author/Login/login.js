import React from 'react';
import axios from 'axios';
import { createBrowserHistory } from 'history';
import {withRouter} from "react-router-dom";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            accounts: []
        }
    }

    
    componentWillMount() {
        axios.get(`http://localhost:3000/accounts`)
            .then(res => {
                const data = res.data;
                console.log(data)
                this.setState({
                    accounts: data
                })
            })
    }

    handleSubmit = () => {
        var result;
        this.state.accounts.forEach((user) => {
            if (user.username === this.state.username && user.password === this.state.password) {result = user;}
        });
        console.log("hihi",result)
        if (result === undefined || result.length === 0) {
            alert('sthing wrong');
            this.props.updateState(false, result.username);
        } else {
            alert('login success');
            this.props.updateState(true, result.username); 
            this.props.history.push('/');          
        }
        // this.props.login(this.state.username, this.state.password, this.state.accounts);
    }
    render() {
        return (
            <div className="container container-form">
                <div className="panel panel-info">
                    <div className="panel-body">
                        <form>
                            <legend>Login</legend>
                            <div className="form-group">
                                <label >Username</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Input field"
                                    value={this.state.value}
                                    onChange={(event) => { this.setState({ username: event.target.value }) }} />
                            </div>
                            <div className="form-group">
                                <label>Password</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    placeholder="Input field" value={this.state.value}
                                    onChange={(event) => { this.setState({ password: event.target.value }) }} />
                            </div>
                            <button
                                className="btn btn-primary mb-2"
                                onClick={() => this.handleSubmit()}>Login</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}


export default withRouter(Login);
