import React from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import Swal from 'sweetalert2';
// import axios from 'axios';
import { Link } from "react-router-dom"



import { register } from '../../../store/action/registerAction';

class Register extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            confirm: ""
        }
    }

    handleRegister() {
        if (!this.state.username || !this.state.password || !this.state.confirm) {
            alert('Error,Data can not empty ')
                // swal({
                //     title: "Error",
                //     text: "Data can't empty",
                //     icon: "error", 
                //     timer:10000
                // });
        } else {
            if (this.state.password !== this.state.confirm) {
                alert('Confirm password incorrect')
            } else {
                // const user = {
                //     username: this.state.username,
                //     password: this.state.password
                //   };
                // axios.post("http://localhost:3000/products", { user })
                this.props.register(this.state.username, this.state.password);
                alert('Success')
            }

            // swal({
            //     title: "Added",
            //     text: "Data was added",
            //     icon: 'success',
            //     timer:3000
            // })
        }
    }

    render() {
        return ( <
            div className = "container container-form" >

            <
            div className = "panel panel-info" >
            <
            div className = "panel-body" >
            <
            form >
            <
            legend > Register < /legend> <
            div className = "form-group" >
            <
            label > Username < /label> <
            input type = "text"
            className = "form-control"
            placeholder = "Input field"
            value = { this.state.value }
            onChange = {
                (event) => { this.setState({ username: event.target.value }) } }
            /> <
            /div> <
            div className = "form-group" >
            <
            label > Password < /label> <
            input type = "password"
            className = "form-control"
            placeholder = "Input field"
            value = { this.state.value }
            onChange = {
                (event) => { this.setState({ password: event.target.value }) } }
            /> <
            /div> <
            div className = "form-group" >
            <
            label > Confirm password < /label> <
            input type = "password"
            className = "form-control"
            placeholder = "Input field"
            value = { this.state.value }
            onChange = {
                (event) => { this.setState({ confirm: event.target.value }) } }
            /> <
            /div> <
            div className = "form-group" >
            <
            Link to = '/login' > Login when register success < /Link> <
            /div> <
            button className = "btn btn-primary mb-2"
            onClick = {
                () => this.handleRegister() } > Register < /button> <
            /form> <
            /div> <
            /div> <
            /div>
        );
    }
}


const mapStateToProps = (store) => {

    return {
        registerAcc: store.authorReducers.accounts,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        register
    }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Register);