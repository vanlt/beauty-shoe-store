import React from 'react';
import { Link } from "react-router-dom";

import './style.css';
import { Cart } from '../Cart/cart'

class Navbar extends React.Component {

    render() {
        var logged = "hide";
        var nonelogin = "show";
        var admin = "hide";
        console.log(this.props.isLogin);
        if (this.props.isLogin === true) {
            logged = "show";
            nonelogin = "hide";
            if (this.props.user === 'admin') admin = "show";
        }

        return (
            <React.Fragment>
                {/* Navbar */}
                <section className="fixed-top mainnav">
                    <div className="container">
                        <nav className="navbar navbar-b navbar-trans" role="navigation">
                            <div className="navbar-header">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                    <span className="sr-only">Toggle navigation</span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                                <Link className="navbar-brand" to="/">Beauty shoe Store</Link>
                            </div>

                            <div className="collapse navbar-collapse navbar-ex1-collapse">
                                <ul className="nav navbar-nav">

                                    <li><Link to="/products">Products</Link></li>
                                </ul>
                                <form className="navbar-form navbar-left" role="search">
                                    <div className="form-group">
                                        <input type="text" className="form-control" placeholder="Search" />
                                    </div>
                                    <button type="submit" className="btn btn-default">Submit</button>
                                </form>
                                <ul className="nav navbar-nav navbar-right">
                                    <li className={logged}><a type="button" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-shopping-cart">(0)</i></a></li>
                                    <li className={nonelogin}><Link to="/login" >Login</Link></li>
                                    <li className={logged}><Link to="#">{"hi! " + this.props.user}</Link></li>
                                    <li className={nonelogin}><Link to="/register">Register</Link></li>
                                    <li className={logged}><Link to="#" onClick={() => window.location = "/"}>Logout</Link></li>
                                    <li className={admin}><Link to="/addProd">Product Management</Link> </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="container">
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Your cart(0)</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>This is a small modal.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </React.Fragment>
        );
    };
}


function Slide() {
    return (
        <div className="container">
            <div id="carousel-id" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators">
                    <li data-target="#carousel-id" data-slide-to="0" className=""></li>
                    <li data-target="#carousel-id" data-slide-to="1" className=""></li>
                    <li data-target="#carousel-id" data-slide-to="2" className="active"></li>
                </ol>
                <div className="carousel-inner">
                    <div className="item">
                        <img src="https://www.desktopbackground.org/download/1024x600/2011/03/27/178477_reliable-index-image-converse-sneakers_1600x1200_h.jpg" className="d-block w-100" alt="..." />
                    </div>
                    <div className="item">
                        <img src="https://www.desktopbackground.org/download/1024x600/2011/07/21/238004_converse-shoes-sneakers-vectors-wallpapers_1920x1200_h.jpg" className="d-block w-100" alt="..." />

                    </div>
                    <div className="item active">
                        <img src="https://images.wallpaperscraft.com/image/feet_sneakers_shoes_grass_116699_1024x600.jpg" className="d-block w-100" alt="..." />
                    </div>
                </div>
            </div>
        </div>
    );
}
export { Navbar };
export { Slide };