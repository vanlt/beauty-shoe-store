import React from 'react';
import { Slide } from "./header";
// import Footer from "./footer";
import { ListProds } from '../Product/list-prods';

class Home extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Slide />
                <ListProds />
                {/* <Footer/> */}
            </React.Fragment>

        );
    }

}

export  {Home};