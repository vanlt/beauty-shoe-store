import React from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom"; 

import {Navbar} from "./header";
// import Footer from "./footer";
import Register  from '../Author/Register/register';
import  Login  from '../Author/Login/login';
import { ListProds } from '../Product/list-prods';
import { Home } from './home';
import addProd from '../Product/addProd';
import { ProdDetail } from '../Product/prodDetail';

class Index extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isLogin : false, 
            user:''
        } 
    }

    updateState = (value, user) => {
        this.setState ({
            isLogin : value,
            user : user
        })
        console.log(this.state.isLogin);
    }

    render() {
        console.log(this.state.isLogin)

        return (
            <Router >
                <React.Fragment>
                <Navbar isLogin = {this.state.isLogin} user = {this.state.user}/>
                    {/* <Footer/> */}
                </React.Fragment>
                <Route path="/" exact component={Home} />
                <Route path="/login" component={() => <Login updateState = {this.updateState}/>} />
                <Route path="/register" component={Register} />
                <Route path="/products" component={ListProds} />
                <Route path="/addProd" component={addProd} />
                <Route path="/prodDetail" component={ProdDetail} />
            </Router>
        );
    }

}

export  {Index};