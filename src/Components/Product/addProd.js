import React from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import swal from 'sweetalert';


import addProd from "../../store/action/addProdsAction";

class Add extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            img: "hihi",
            category: "",
        }
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleLogin() {
        if(!this.state.title || !this.state.description || !this.state.img || !this.state.category){
            alert('Error,Data can not empty ')
            // swal({
            //     title: "Error",
            //     text: "Data can't empty",
            //     icon: "error", 
            //     timer:10000
            // });
        }else {
            this.props.addProd(this.state.title, this.state.img, this.state.description, this.state.category);
            alert('Success,Data was added')
            // swal({
            //     title: "Added",
            //     text: "Data was added",
            //     icon: 'success',
            //     timer:3000
            // })
        }
    }
    render() {
        return (
            <div className="container container-form">
                <div className="panel panel-info">
                    <div className="panel-body">
                        <form>
                            <legend>Add New Product</legend>
                            <div className="form-group">
                                <label>Title</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Input field"
                                    value={this.state.value}
                                    onChange={(event) => { this.setState({ title: event.target.value }) }} />
                            </div>
                            <div className="form-group">
                                <label>Description</label>
                                <textarea
                                    rows="3"
                                    type="text"
                                    className="form-control"
                                    placeholder="Input field" value={this.state.value}
                                    onChange={(event) => { this.setState({ description: event.target.value }) }}></textarea>
                            </div>
                            <div className="form-group">
                                <label>Image</label>
                                <input
                                    type="file"
                                    className="form-control"
                                    placeholder="Input field" />
                            </div>
                            <div className="form-group">
                                <label>Category</label>
                                <select className="form-control" value={this.state.value}
                                    onChange={(event) => { this.setState({ category: event.target.value }) }}>
                                    <option value="Nike">Nike</option>
                                    <option value="Balance">Balance</option>
                                    <option value="Vans">Vans</option>
                                    <option value="Adidas">Adidas</option>
                                </select>
                            </div>
                            <button className="btn btn-primary" onClick={this.handleLogin } >Add</button>

                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (store) => {
    console.log(store)
    return {
        // prodInfor: store.prodReducers.prods,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        addProd
    }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(Add);
