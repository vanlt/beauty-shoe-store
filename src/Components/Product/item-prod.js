import React from 'react';
import './product.css'
import { Link } from "react-router-dom";

import {ProdDetail} from './prodDetail'

class ItemProd extends React.Component {

    render() {
        return (
            <React.Fragment>
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div className="thumbnail">
                        <img className="img" src={this.props.item.img} alt="" />
                        <div className="caption">
                            <h4 className='title'>{this.props.item.title}</h4>
                            <p className="des">
                                {this.props.item.description}
                            </p>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <span class="tag">${this.props.item.price}</span>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <Link to="/prodDetail" className="btn btn-primary">View</Link>&ensp;
                                    <Link to="#" className="btn btn-default" ><i class="glyphicon glyphicon-shopping-cart"></i></Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export { ItemProd };
