import React from 'react';
import axios from 'axios';
import {ItemProd} from './item-prod'

class ListProds extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            products:[]
        }
      }

    componentWillMount() {
        axios.get("http://localhost:3000/products") 
        .then(response => {
            this.setState({ products: response.data });
            })
        .catch(err => console.log(err));
    };

    
    render() {
        return (
            <React.Fragment>
                <div className="container">
                    {this.state.products.map((item) => (<ItemProd item = {item} key={item.id}/>))} 
                </div>
            </React.Fragment>
        );
    }
}
export {ListProds};