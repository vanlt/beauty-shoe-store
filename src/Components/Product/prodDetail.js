import React from 'react';
import './product.css'
import { Link } from "react-router-dom";


class ProdDetail extends React.Component {

    render() {
        return (
            <React.Fragment> 
                <div class="container">
                    <div class="row">
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDw8PDxAPDw8PEA8PDw8PFRUVDw8PFRUWFhUVFRUYHSggGBomHRUVITEhJSktLi4uFyAzODMtNygtLisBCgoKDQ0OFQ8PFS0ZFRkrKysrKysrKy0rLTcrKys3LS03Nys3LS0rNzcrKysrKzc3NysrKystLS03NzcrKy0tN//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIDBAUHBgj/xAA/EAABAwIEAwUFBQcCBwAAAAABAAIDBBEFEiExBkFREyIyYXEHFEKBoSNScpGxM0NiwdHh8FPxFSRUkpOi0v/EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/EABYRAQEBAAAAAAAAAAAAAAAAAAABEf/aAAwDAQACEQMRAD8A6eiIgIiICIiAiIgIsDFcZpaQA1M0cVxdrXXL3DqGi7iPQKcLximqwTTytktqQLhwB2Ja4AgfJBnIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIi8xxzxazDYHObkfUub9jE8nLqQMzrbganKLE5TsASA22OY1BRRGWd1h8LBbPIejR/PZcv4x9pFQ8GCnBpQcoe5rrza7gPHgNvu6g7FeLxvG56p5lnkMkj9CdAALbNA0A12C1OIVLpXukdYF7sxA2HoqM6GulnmaC4uBPezG5t1udV0LgCqJxCAM0aRK0+bRE82PzaD8gud4czJFn+KQlt+jRv+o/Mre4NibqeWOWM2fG4OHT0PUHb5oj6CRYOC4pHWQMniOjtHN5sePE0+n1FjzWcooiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgJ/si1VPiuaeoYNoXNjHk7IHOP/tZBb4t4hiw2mfPJq/wwxbGWU3yt8hoSTyAK+csbxSWrnkqJnZpJDcnkBya3oB0XQvbdO90tPqchLyOmYMYB87X+q5a4oM2reCI7Zf2bc2W3i1ve3PbfVY0rSLXBFwHC+l2nYjyWRUG8UBN/A5l+oa93/0q8R1jpX9YOz/8b3N/SyC7h77xFvON1x+E6H62/JZsa1GHzZJBfwu7rvQ6LatuCR0+qqPWcE8SOopu8SYJLNlb5cngdR9RcLtEUrXta9hDmOAc1zdQ5pFwQV85NK9lwVxqaO0FRmdTE6EaugJ3IHNvMj5joSuvIrNHVRzMbJE9skbvC9hu0/38leUBERAREQEREBERAREQEREBERAREQEREBeBmrjBimJREeMUs8fmHR5Xn/ubZe+XPuO6bJiNFUaNbM00MrzewLyXwD5vbID0zBWDS8Zs/wCJ4fLIxpE1FM+7NzZniGm92ODh6hcjX0JhWEtgfM8EuM4jztsAy7QQHAfeILQT5N6Lj/HfD5oqp2UfYTEviI2B3Lf5jyPklRpmWMB6skHP4XNPK9jq3pf5K9J3qSM/6VRLGPwvY1/6hywoeazqEZoKuPmGxTj0jfld9JPoorXlbalqO0AH7xosR98DYjzWqslrahBvWPV3NdamCvO0gv8AxDxfPqs+KQO1Y4O9N/yVRtcHxypon56eQsv4mHWN/wCJp0PruORC6TgXtMpZWkVbXU8rRe7GukjkP8OUFwPkR8yuLz11rhupGhJ2urcU7nbnToNAivpHAeIaauEhgLrxZc7Xts5odmynprlPO4trZbZcm4YxWDCY2Sydo90opPe9gymgmD3RvaN3EHs834xbmusNcCAQQQQCCNQQdiClEoiKAiIgIiICIiAiIgIiICIiAiIgLU8T4GyvppKd7jGXDuSDUxvBDmutcbOa06EXy2vqVtkQammw17Yx2sjXvawZ3NGVrnAanU6Am5tyvvovP8XYC2vpnQ2Af4onW8Emtj+t/LMFm8c1lRS+71cZL6aFzxWU4GroXZftW9XR2vboXbAErScRcWPp4mVFOxs8Qcx0hG7qdw7zmH0tqeWp5qo4m+F8Ujo5Glj2OLHtO7XdFW2RzC7KbZ2uY7bVjh3hqui+0PAI6uFmK0djmYJJbaGSLLcPt95o3HQEcgFzmUaoq0pQqFAsqbWOmnmN1N1BQXveL/tGh/8AFtIB+Ib/ADV6NgILozmDdXA6PaOpHMeY+iyuHKVjpO1kBcyB0TjGLd8ucbXvyGU/TZZnERY2tNREPsZBG4d3LezRHICPOzna73Pmg3tHK+swuaN7oGObkp4CIz29SYGdsyF8l7NaAQAbaktHr7/2V46JqOOjkuKmka6NwI0MTXlrLOGhLRlaR5c1zDCKt4L2MYyMRTMeJf3QY0nM14+Iuszbf9fa+y6sigqn0QpywviEkUpN3FgzXzj4S7KTbkGjrpqo6iiIsqIiICIiAiIgIiICIiAihEEoiICIiC1UQCRpa7Y+hsfQ6H0OhFwbglc/pKCPDav3WoDXUlS4+5vfctgmOppzc6sduwnXdpJOq6KocAdCAfI7INWaVpjy2aGWsA21rcrAf5+a4LxvgJoaySMNtDJ9pAR4ezO7R+E6W6FvVduGOB1VUUszewlgtJGHG4npnWAlafW7SORsOenm8fmosYdPhgOSsgaJoJHDTNa9mnmLEZh0fcbaVHEis+noYC1rpatkTn7MbG95brbvnQN+qx6ymfFI+KVpZJG4sew7tcP835q5Q1rYQT2EUslwWSSlxawafuwbE31uitnFh0kTSyOniqZ2zSR1Ae3tOzAt2dhcZWuBJzfUWWsxqkZFM9kZa6M96MtcHgNJIsSOYII110WLPK6R7nvOZ7yS5x5k7/7K2oLlPUvj1Y4tNrG3P1BWSwscQ+aV0rna9m08zycdm8tBtZYKv0zYh3pHOuDpGwd426uOiD0FBiEs1o4Y2xhouXbtZp4r8ufmVuKCvqaKoh91yl8uftnyC8zhYAyuuDkaBoL6/wAvPUddM8iCnY2NtzYN3A+85x/NbStgbHBK3tHB2Vz3lh+1meBcNP3WXt5kLQ+gozcA9QCqlq+Fw33GkLJXztdBG9sr/E5rhmF+lr2tytZbRZBECICIiAiIgIiICIiAiXRAREQEREBERBpOKcAFbGx0bhFVwEvpZj8LiO8x3VjtiP6Lz3DGHUs8vvMkBZiNI4wzRuJD4JLHYA2cCCS12twdOa94sLEHRU7Z6wwl8jISXmJgM8kcYLgwczzsL2Qcz9r3DN2jEIhqyzKgDmz4X/LUHyPRq5M5fTFRW0s0LS+SN0FS1rWEkZJBJYNbr1uBr1t5Lg/G/Dj8NqCyznQSXdTyH4m6XafMXt5ixQecKhC5RdBJVyBrCT2jnNA2yi5d5eStXRBtG19gGQjsmEXcQftH8u875bfVZtBLfQ2tu4nmP8t8rrV0kJGVxBaHmzHnQZhuL/5t6rNibkcCL203H0VHePZ1LEcNp2R3HYh0b2OOZzH5i4g9BZwIHIEDkvSrlvsrxJrKmWB29SxrmG5sHx5iG5b7lpcb2+DzXUlAREQEREBERAREQEREBERAREQEREBERAQFEQeCxfh5lPP2JsMNrnuYARdlLUyEkxW5RyEnLyDjbS4Wbxnwz77h5guXzRNDopX+J0jRYOcR11BtycSvVVlLHNFJDK0PjlaWPadi0/oeYO4KssqY2P8AdsxMjYmyHPcufHcszFx0cbjXpmF7Zhcj5WljcxzmOBa5pLXNO7XA2IPzCoXQfa3w52FQK2LWCosHluzZbaEn+IfVp6rn6KIpQINscUDoTGWgucMpHwi2zx0I5f00U0dSHgNdqRb5i9r689VqrrdQVEPYEEBvOw1f2oGhHkb29CqM1rbxlhOhaQTtoQQfRd14Pq5JsPo5ZnskkfAwvewkhxta5uB3tO8LaOuF83yVbn6Hb7o2/uve+y3jD3OT3Ood/wArO/7J52p5ndejHHfodebig7WiIoCIiAiIgIiICIiAiIgIiICIiAiIgIiIC85xfQyODKiIgSQggFxIa2/Jx5ROuWPPLMx50iXo1BF9DsdD0IQeQpMNbX0MsUrbwyh2RjtJYpQ4h7CPhc17eezgVwnGMJko55KeUd5h0dawez4XjyI/mOS+kmUkVGZ5w57YiGvkjy5mNLRlzgAZvCGg+TB0XnuOOGqbFGtDZGMqcueB7bHMCL7/ABNIsbfMKo+fnKFlYths1JM+CdhZIzlycOTmnmPP+axFFV3/AFVTSrX9VW0oLrVcaeR1B5KyCqwUHX/Znx2HCOgrH97RlLUPPj5NikJ+Lk13xbHW2bqC+Uwev5dV2D2Y8dmbJQVj7y6Npp3HWUco5D9/o74tjr4g6YiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgLyeJYD2D+1ha99NmzSU8X7WE3v2lPblfUxj1Zr3D6xEHmOKOF6bE4AZG62DopRpI243F9tALg6G2vlwniPhqpw+TJUMOUm0cwB7OT06H+E6+o1X0pXxyuaDC8Ne03s5ocyQc2u1BHkQRY23Gh8sXTSP7Csjj7KQhskc9nQPB5RSEC7ujXAONvCN0Hz7lVNrLsHEPsojcHSUT+xP8ApSXdET0B8TfXXyC5zjPDdZRk+8QPa0fvGjNFbrmG3zsg1VPMGPa4gnKQbNIBuNrEggfMFZ1XUU8kYLI+ylzi4Fy0ss65uLN1OTQNbYg20Nhr8qpyIMiMX52WZRQ94OJ1BBblJBBGxvuCFrm3WzwiimqJWQwi73kAdBfmfJWD6P4erXVFHTTvtnlhjc8jYvt3iOgvcrYLGw2jbTwQwM8MMUcTSdyGNDbnz0WSoCIiAiIgIiICIiAiIgIoul0EoouEzIJRU5lOZBKKLqUBERAVL2BwIcA4EEEEXBB3HoqkQY81McjWRO7LJbKGtaWWAIDS37uvwkHQa9dA/D66OTM7LURXGZsZGZ45gRSkBvyk/v6dEHhMc4HwyRnbSwmnLnAF0Yc1zXHm4M0JvzNxr5ryNb7NacXMWIBgH/UR309W5bfku0qkMA2AHoEHDx7NJnWEFRHUuJGscTxAGndzpnHLp0FztpzXSuDeC4MObm/a1Dh35Tt6NHIL1BKJoIiICIiAiIgIiICIiCLqCUKpcUAuVsvVLyrL3ILplVJmWM5xVpzigze3QTrAzpnQbITKoTLWiRSJUGz7VSJFrhMqhOg2IepDlrveFUKpBsMym614qwrjasIM1FjNqQrgmCC6ipD1VdAREQESyICIiAiIgIiIKSqSFXZLILDmqy5iy8qgsQYDo1bdEtiY1BiQawxqkxrZGFUmBBrsqZVnGBUGBBh5VOVZBiUdmgx8qZVkZFGRBYyplV/IpyILGqrDyFXkTIglk5V+OpVjIqmRoM5k11dBWLEwrJaEFSIiAiIgIiIIRSiAiIgIiIIsllKIIsosqkQU5VGRVogtmNUmIK8iDH7FOwWQiDG7BOwWTZEGN2CnsAshEFkQhVCIK4iCA1SiICIiAiIgIiIIRSiAiIgIiICIiAgREAKERBKIiCCpCIgIiICIiAoCIglQURBKIiAiIgIiICIiD//Z" alt="Image"/>
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Media heading</h4>
                                    <p>Text goes here...</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            ahihi
                        </div>
                        
                    </div>
                    </div>
            </React.Fragment>
        );
    }
}

export { ProdDetail };
