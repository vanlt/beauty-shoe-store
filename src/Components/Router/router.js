import React from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom"; 

import Home from '../Home/home'
import { Register } from '../Author/Register/register';
import { Login } from '../Author/Login/login';
import { ListProds } from '../Product/list-prods';
function AppRouter() {
    return (
        <Router>
            <Route path="/" exact component={Home}/>
            <Route path="/login"  component={() => <Login />}/>
            <Route path="/register"  component={Register}/>
            <Route path="/products"  component={ListProds}/>
        </Router>
    );
    };

export {AppRouter};