import React, { PureComponent } from 'react';
import swal from 'sweetalert';


class Alert extends PureComponent {
    render() {
        return (
            swal({
                title: this.props.title,
                text: this.props.text,
                icon: this.props.icon,
            })
        );
    }
}

export { Alert };