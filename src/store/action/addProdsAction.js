import axios from "axios";

const addProd = (title, img, description, category) => {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            dispatch({
                type: "ADD_PROGRESS",
            });

            axios.post("http://localhost:3000/products", { title, img, description, category })
                .then((response) => {
                    setTimeout(() => {
                        if (response.data.success) {
                            dispatch({
                                type: "ADD",
                                data: {
                                    title: title,
                                    img: img,
                                    description: description,
                                    category: category
                                }
                            });
                            resolve();

                        } else {
                            dispatch({
                                type: "ADD_FAILED",
                            });

                            reject();
                        }
                    }, 3000);
                })
                .catch((err) => {
                    dispatch({
                        type: "OPERATION_FAILED",
                    });
                    reject();
                })
        });
    }
}

export default addProd;