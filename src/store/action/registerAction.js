import axios from "axios";

const register = (username, password) => {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            dispatch({
                type: "REGISTER_PROGRESS",
            });

            axios.post("http://localhost:3000/accounts", { username, password })
                .then((response) => {
                    setTimeout(() => {
                        if (response.data.success) {
                            dispatch({
                                type: "REGISTER",
                                data: {
                                    username: username,
                                    password: password
                                }
                            });
                            console.log(response.data.success);
                            resolve();
                        } else {
                            dispatch({
                                type: "REGISTER_FAILED",
                            });
                            reject();
                        }
                    }, 3000)
                })
                .catch((err) => {
                    dispatch({
                        type: "OPERATION_FAILED",
                    });
                    reject();
                })
        });
    };
}

export { register };