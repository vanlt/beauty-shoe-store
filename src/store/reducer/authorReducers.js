import axios from 'axios';

const request = axios.get('http://localhost:3000/accounts');
const initialState = {
    accounts: request,
    action: ''
};

const authorReducers = (state = initialState, action) => {
    state = {...state, lastAction: action.type };
    switch (action.type) {
        case "REGISTER":
            return {
                ...state,
                accounts: [
                    ...state.accounts,
                    { username: action.data.username, password: action.data.password }
                ]
            };
        default:
            return state;
    }
}

export { authorReducers };