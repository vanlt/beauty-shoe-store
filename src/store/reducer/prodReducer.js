import axios from 'axios';

const request = axios.get('http://localhost:3000/products');
const initialState = {
    prods: request,
    action: ''
};

const prodReducers = (state = initialState, action) => {
    state = {...state, lastAction: action.type };
    console.log(initialState);
    switch (action.type) {
        case "ADD":
            return {
                ...state,
                prods: [
                    ...state.prods,
                    { title: action.data.title, img: action.data.img, description: action.data.description, category: action.data.category }
                ]
            };

        default:
            return state;
    }
}

export { prodReducers };