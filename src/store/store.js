import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

import { prodReducers } from './reducer/prodReducer';
import { authorReducers } from './reducer/authorReducers';

const configStore = () => {
    return createStore(
        combineReducers({
            prodReducers: prodReducers,
            authorReducers: authorReducers,
        }),
        applyMiddleware(thunk)
    );
}

export { configStore };